# RevisedPlayground

The RevisedPlayground mod is just a catch-all project for little tweaks and additions to Minecraft 1.20.x via the Forge ModLoader.  
  
## Current Offerings
  
#### Clear Glass Blocks

Much more clear blocks can be made by placing two normal glass blocks anywhere in the crafting grid to create 2 blocks that are much more clear (and harder to see). Harvesting these blocks drops them for re-use unlike standard minecraft glass that shatters and disappears.  
  
#### Nametag Recipe
  
Nametags can be crafted with one iron nugget, one plank block of any basic minecraft type, and one string.  


